import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Links extends Component {
	render() {
		return (
			<div>
				<ul>
					<li><Link to='/'>Home</Link></li>
					<li><Link to='/lobby'>Lobby</Link></li>
				</ul>
			</div>
		);
	}
}
export default Links;