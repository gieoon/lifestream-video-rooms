import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Video from '../webrtc/video';
import VideoContainer from './videoContainer';
import './tablePage.css';
import { Row } from 'reactstrap';
//error because of 'state' instead of 'this.state'


//has ten mini windows, and the videos will be in these locations.

//table has already been created in server, 
//only have to show this to user.
class TablePage extends Component {
	constructor(props){
		super(props);
		this.state = {
			cardNo: 0,
    		tableId: 'loading',
    		users: [],
    		sockets: [],
    		currentFood: '',
    		bDisconnect: false,
    		bInitiator: this.props.match.params.bInitiator == 1 ? true : false,
    		videoContainers: [],
    		rowContainers: [],
    		size: this.props.match.params.size
  		};
  		
  		//console.log("table props: ", this.state.videoContainers);
  		this.HandleBackButton = this.HandleBackButton.bind(this);
  		this.initContainers = this.initContainers.bind(this);
  		this.initRows = this.initRows.bind(this);
	}
	
  	componentDidMount(){
  		//console.log("params: ", this.props.match.params.tableId);
  		this.setState({ 
  			tableId: this.props.match.params.tableId
		});
		this.initContainers();
  	}

  	HandleBackButton(){
  		//console.log("handling back");
		this.setState({ bDisconnect: true });		
  	}

  	initRows(){

  	}

  	initContainers(){
  		let tempContainers = [];
  		for(var i = 0; i < this.state.size; i++){
			tempContainers.push(
				<VideoContainer 
       				key={i + 1} 
       				position={i + 1}
       				bInitiator={this.state.bInitiator}
       				id={this.props.match.params.tableId}  
       				bDisconnect={this.state.Disconnect}>
   				</VideoContainer>
			);
  		}

  		this.setState({ videoContainers: tempContainers });
  		///console.log("videoContainers: ", this.state.videoContainers)
  	}

	render() {
		const row1 = this.state.videoContainers.slice(0, this.state.size / 2).map(
	      	(videoContainer) => videoContainer
      	);
      	const row2 = this.state.videoContainers.slice(this.state.size / 2, this.state.size).map(
	      	(videoContainer) => videoContainer
  	  	);

	    return (
	      <div className="Title">
	        <p>Linked to table</p>
	       	<p>You are on table: { this.state.tableId }</p>
	       	<div className="videoHolder">
	       	
	       		{ row1.map((videoContainer, index) =>
	       			videoContainer
	       		)}

	       	</div>
	       	<div className="videoHolder">
	       		{  row2.map((videoContainer, index) =>
	       			videoContainer
	       		)}
	       	</div>
	       	<p>With:</p>
	       	<ol>
	       		{this.state.users.map(user => <li>{user.username}</li>)}
	       	</ol>
	        <Link to='/' onClick={this.HandleBackButton}>Back</Link>
	      </div>
	    );
 	 }
}
//<Row>
 ///<p>Table Number: {props.number} </p> //this.state.id
 //{ this.state.videoContainers.map((videoContainer, index) => ) }
export default TablePage;