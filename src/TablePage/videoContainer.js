import React, { Component } from 'react';
import Video from '../webrtc/video';
import './videoContainer.css';
import { Col } from 'reactstrap';

class VideoContainer extends Component {
	constructor(props){
		super(props);
		this.state = {
			id: this.props.position
		};
	}

	componentDidMount(){

	}

	//only create a video for the first user
	render(){
		return(
			<Col xs="3" className="videoContainer">
				<span className="userDetails">User</span>
				{ this.state.id == 1 ?  <Video id={this.props.id}></Video> : '' }
			</Col>
		);
	}
}

export default VideoContainer;