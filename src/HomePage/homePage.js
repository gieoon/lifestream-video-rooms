import React, { Component } from 'react';
import Title from '../LandingPage/title.js';
import Login from '../Authentication/login.js';

import Carousel from '../Carousel/carousel';

class HomePage extends Component {
	render() {
	    return (
	      <div className="Title">
	        <p>Linked to Home</p>
	        <Title />
    		<Carousel />
        	<Login />
	      </div>
	    );
 	 }
}

export default HomePage;