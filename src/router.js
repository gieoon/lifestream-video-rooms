import React from 'react';
import { Switch, Route } from 'react-router-dom';
import TablePage from './TablePage/tablePage';
import LobbyPage from './LobbyPage/lobbyPage';
import HomePage from './HomePage/homePage';

//defines what component each page points to.
//if a url is structured in this way, deliver this component.
//if a url has /table/followed by something else, treat those parts as what is defined here.
const Routes = () => (
	<main>
		<Switch>
			<Route exact path='/' component={HomePage}/>
			<Route path='/table/:tableId/:size/:bInitiator' component={TablePage}/>
			<Route exact path='/lobby' component={LobbyPage}/>
		</Switch>
	</main>
);

export default Routes;