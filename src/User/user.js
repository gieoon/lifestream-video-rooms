import React { Component } from 'react';
import './user.css';

class User extends Component {

	constructor(props){
		super(props);
		this.state = {
			id: Date.now(),
			username: '',
			currentTableId: 0
			pastTableIds: [],
			currentFood: '',
			pastFoods: [],
			location: ''
		};
	}

	componentWillMount(){

	}

	componentDidMount(){

	}

	render(){
		return(
			<div>
				<span>{this.state.username}</span>
				<span>is eating {this.state.currentFood}</span>
				<span> at {this.state.location}</span>
			</div>
		);
	}
}

export default User;