import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Routes from './router';
import Links from './links.js';

import bs from './bootstrap/css/bootstrap.min.css';
import fa from './font-awesome/css/font-awesome.min.css';



class App extends Component {
  render() {
    return (
      <div className="App">
        <Links />
        <header className="App-header">
          
          <p><i>
            You need to keep finding new ways to tell your story and sell your personal brand—increasingly with pictures and graphics.
          </i></p>
        </header>
        <Routes />
      </div>
    );
  }
}

///<img src={logo} className="App-logo" alt="logo" />

export default App;
