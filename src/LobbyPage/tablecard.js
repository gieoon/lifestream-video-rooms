import React, { Component } from 'react';
import './tablecard.css';

//component to display table with four users and a food in lobby screen
//is offset in lobby screen based on card number.
class TableCard extends Component {
	constructor(props){
		super(props);
		this.state = { 
			cardNo: '',
			users: this.props.users,
			currentFood: this.props.currentFood,
			startedAt: this.props.startedAt
		};
		//console.log("tablecard props: ", props);
		//console.log("this.users: ", this.state.users);
	}

	render(){
		return(
			<div className="tablecard">
				<p>eat {this.state.currentFood} with</p>
				{ Object.keys(this.state.users).map((index) =>
					<span key={index}>
						{index > 0 ? ', ' : '' } 
						{this.state.users[index].username}</span>
					
				)}
				<p>since: {Date.now() - this.state.startedAt} ago</p>
			</div>
		);
	}
}
///{ this.state.users.map((user) => <p key={user.id}>{user.id} with: {user.username}</p>)}
export default TableCard;