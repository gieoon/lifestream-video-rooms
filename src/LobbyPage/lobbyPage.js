import React, { Component } from 'react';
import './lobby.css';
import { Link } from 'react-router-dom';
import TableCard from './tablecard';

class LobbyPage extends Component {

	constructor(props){
		super(props);
		this.state = {
			tables: {},
			newTable: null
		};
		this.RequestTable = this.RequestTable.bind(this);
		this.CallNewTable = this.CallNewTable.bind(this);
		this.GetCurrentTables = this.GetCurrentTables.bind(this);

	}


	componentDidMount(){
		//query the lobbies that are currently running.
		this.GetCurrentTables();
		//show them as a list.
		//3 x 3 grid of the lobbies, with players around them.
	}

	GetCurrentTables = () => {
		this.CallCurrentTables()
			.then(res => {
				this.setState({ tables: res.tables })
				//console.log("this.state.tables: ", this.state.tables)
			})
			.catch(err => console.log(err));
	}

	CallCurrentTables = async() => {
		const response = await fetch('/api/getCurrentTables');
		const body = await response.json();
		if(response.status !== 200) throw Error(body.message);
		return body;
	}

	RequestTable = () => {
		//console.log("Requesting a new table");
		this.CallNewTable()
  			.then(res => {
  				this.setState({ newTable: res.table })
  				//console.log("this.state.newTable: ", this.state.newTable);
  			})
  			.catch(err => console.log(err));  //without catching this program crashes and doesn't display.
		//console.log("Props: ", this.state.newTableId);

	}

	CallNewTable = async() => {
  		const response = await fetch('/api/getNewTable');
  		const body = await response.json();
  		//console.log("Received response from server: ", body.tableId);
  		if (response.status !== 200) throw Error(body.message);

  		return body;
  	};
/*
users={table.users} 
	        				currentFood={table.currentFood}>
	        				startedAt={table.startedAt}
*/
	render() {
		
		const {tables} = this.state;

	    return (
	      <div className="Title">
	        <p>Linked to Lobby</p>
        	<div>
	        	{
	        		Object.keys(this.state.tables || {}).map((key) => (
	        			<TableCard 
	        				key={this.state.tables[key].uniqueId} 
	        				users={this.state.tables[key].users}
	        				currentFood={this.state.tables[key].currentFood}
	        				startedAt={this.state.tables[key].startedAt}>
	        					tablecard
	        			</TableCard>
	        		))
	        	}
        	</div>
	        <div>
		        { this.state.newTable !== null ? 
		        	<Link to={'/table/' + this.state.newTable.uniqueId + "/" + this.state.newTable.size + "/" + this.state.newTable.bInitiator } >Look at Table</Link>
	        		:
	        		<button id="findNewTable" onClick={this.RequestTable}>Find A New Table</button>
			 	}
        	</div>

	        <Link to='/'>Back</Link>
	      </div>
	    );
 	 }
}
	 ///	{ this.state.tables.map(table => <div className="tablecard" key={table.props.cardNo}>{table}</div>)} 
	       //table object is created in router, defining a location as an object.
        
export default LobbyPage;