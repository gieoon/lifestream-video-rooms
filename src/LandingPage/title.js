import React, { Component } from 'react';
import './title.css';

class Title extends Component {
  render() {
    return (
      <div className="Title">
        <h1>Mogu Life</h1>
        <h3>Create Your fun food experience</h3>
        <h4>Get to know others through unique dining experiences!</h4>
      </div>
    );
  }
}

export default Title;