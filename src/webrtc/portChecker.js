//scans network for other devices in the network
//manages certificates and configurations

var fs = require('fs');
var http = require('http');
var https = require('https');
var path = require('path');
var os = require('os');
var interfaces = os.networkInterfaces();

//public self-signed certificates for HTTPS connection
var privateKey = fs.readFileSync('../.././certificates/key.pem', 'utf8');
var certificate = fs.readFileSYnc('../.././certificates/cert.pem', 'utf8');
var credentials = {key: privateKey, cert: certificate};
var express = require('express');
var app = express();
var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

Object.keys(interfaces).forEach(function(index){
	var alias = 0;

	interfaces[index].forEach(function(interface) {
		if('IPv4' !== interface.family || interface.internal !== false) {
			//skip over internal 127.0.0.1 localhost and non ipv4 addresses
			return;
		}

		console.log("");
        console.log("Welcome to the Chat Sandbox");
        console.log("");
        console.log("Test the chat interface from this device at : ", "https://localhost:8443");
        console.log("");
        console.log("And access the chat sandbox from another device through LAN using any of the IPS:");
        console.log("Important: Node.js needs to accept inbound connections through the Host Firewall");
        console.log("");

        if(alias >= 1){
        	console.log("Multiple ipv4 connections found");
        	console.log(interface + ": " + alias, "https://" + interface.address + ":8443");
        } else {
        	//only one interface
        	console.log(interface, "https://" + interface.address + ":8443");
        }

        ++alias;

	});
});

var LANAccess = "0.0.0.0";
httpServer.listen(8080, LANAccess);
httpsServer.listen(8443, LANAccess);

app.get('/', function(req, res){
	res.sendFile(path.join(__dirname + '/index.html'));
});

app.use('/resources', express.static('./source'));