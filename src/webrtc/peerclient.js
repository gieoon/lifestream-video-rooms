import React, { Component } from 'react';
import './video.css';
//import Peer from 'peerjs'; //can't do this...wrong library
import peer from 'simple-peer';
import io from 'socket.io-client';

//IMPLEMENTATION USING PEERJS WITH A SERVER TO ACT AS A CONNECTION BROKER

class PeerClient extends Component {
	constructor(props){
		super(props);
		this.state = {
			//peer: {},
			id: '',
			peerId: '',
			initialized: false,
			bAudio: true,
			bVideo: true
		}
	}

	componentDidMount(){
		const socket = io.connect();
		this.initPeer();
	}

	initPeer(){
		const peer = new Peer('randomId', { host: 'localhost', port: 5000, path: '/peer' });
		console.log("peer: ", peer);
		this.setState({peer: peer });
		this.state.peer.on('open', (id) => {
			console.log("my peer id is: " + id);
			this.setState({
				id: id,
				initialized: true
			});
		});
		
		console.log("initialized peer");
	}

	requestLocalVideo(){
		// Monkeypatch for crossbrowser getusermedia
    	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

    	// Request audio and video
    	navigator.getUserMedia({ audio: this.state.bAudio, video: this.state.bVideo }, callbacks.success , callbacks.error)
    		.catch(e => alert('Could not connect to media device: ' + e.name));

	}

	render(){
		return(
			<div className="video-wrapper">
				<div className="local-video-wrapper">
					<video autoPlay id="localVideo" muted ref={video => (this.localVideo = video)}></video>
				</div>
				<video autoPlay id="remoteVideo" ref={video => (this.remoteVideo = video)}></video>
				{this.state.full ? 'The room is full' : ''}
			</div>
		);
	}
}

const callbacks = {
	success: "success",
	error: "error"
};

export default PeerClient;