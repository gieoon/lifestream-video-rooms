import React, { Component } from 'react';
import simplePeer from 'simple-peer';
import './video.css';
import io from 'socket.io-client';

//using a library for simplicity
//import SimpleSignalClient from 'simple-signal-client';

//Deprecated due to not using simple-peer and socket-io-client anymore, have now delegated this work to serverside.
//REACTIVATED and using custom components over PeerJS
class Video extends Component {
	constructor(props){
		super(props);
		this.state = {
			localStream: {},
			remoteStreamUrl: {},
			streamUrl: '',
			bInitiator: this.props.bInitiator,
			peerId: 0,
			peer: {},
			full: false,
			bDisconnect: false,
			bAudio: true
		}
		this.end = this.end.bind(this);
		this.sayHi = this.sayHi.bind(this);
		//this.enter = this.enter.bind(this);
		//console.log("this.props: ", this.props);
	}

	componentDidMount(){
		const socket = io('localhost:5000'); //process.env.REACT_APP_PORT //http://localhost:5000
		socket.connect();
		//console.log("socket: ", socket);
		const component = this;
		this.setState({ socket: socket });
		//console.log("props.id: ", this.props.id);
		const { roomId } = this.props.id; //this.props.match.params//this room id is alsothe attached parameters in the url.
		this.getUserMedia().then(() => {
			console.log("emitting 'join'");
			socket.emit('join', { roomId: this.props.id });
		});

		socket.on('init', (data) =>{
			component.setState({ bInitiator: data.initiator, peerId: data.peerId });
			console.log("received 'init' from server: ", data);
			component.enter(data.peerId);
			//create video room!!!
			
			//if inittiaor is true, ask the server for the offer to connect to the host
			if(!data.initiator){
				socket.emit('requestOffer');
			}
		});

		//server replied with offer.
		
		socket.on('offer', data => {
			//connect peer to this offer
			console.log('OFFER received');
			component.call(data);
		});
		
		
/*
		socket.on('signal', (data) => {
			console.log("'signal' : " + data + " received" );
			if(this.state.peer != null){
				//initiate communication with peer
				console.log('SIGNALLING PEER');
				this.state.peer.signal(data.signal); 
			}
		});

*/
		socket.on('desc', (data) => {
			console.log("received DESC: ", data );
			if (data.type === 'offer' && component.state.bInitiator) return;
			if (data.type === 'answer' && !component.state.bInitiator) return;
			component.call(data);
		});
		socket.on('disconnected', () => {
			component.end();
		});
		socket.on('close', () => {
			component.end();
		});
	}

	componentWillReceiveProps(props){
		//console.log("super Props received: ", this.props);
		if(this.props.bDisconnect){
			this.end();
		}
	}

	getUserMedia(cb){
		return new Promise((resolve, reject) => {
			//navigator object contains information about the browser.
			navigator.getUserMedia = navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
			const op = {
				/*video: {
					width: { min: 160, ideal: 640, max: 1280 },
					height: { min: 120, ideal: 640, max: 720 }
				},*/
				video: true, 
				audio: this.state.bAudio
			}
			
			navigator.getUserMedia(op, stream => {
				this.setState({
					streamUrl: stream, 
					localStream: stream
				});
				this.localVideo.srcObject = stream;
				resolve();
			}, () => {});
			
		});
	};

	//initiate the video call
	//connect => signal => (talk to peer)
	enter = (peerId) => {
		console.log("entering room: ", this.state);
		const options = {
			initiator: this.state.bInitiator,
			trickle: true
		}
		//trickle: true
		const component = this;
		//this.setState({ peer: new simplePeer(this.state.localStream, this.state.bInitiator) },  
		this.setState({ peer: new simplePeer(options) },  
			() => { 
				
				console.log("peer: ", this.state.peer);
				this.state.peer.on('signal', data => {
					//comuunicate p2p connection with signalling server. 
					//console.log("peer emitting signal");
					const signal = {
						peerId: peerId,
						desc: data
					};
					component.state.socket.emit('signal', signal);
					//component.call(data);
				});
			});

/*
		this.state.peer.on('signal', function(data){
			console.log("Advertising signalling data");
			this.state.socket.emit('signal', {
				signal: data,
				peerId: peerId
			});
		});
*/		

		this.state.peer.on('connect', function(){
			console.log("PEER CONNECTED");
			component.state.peer.send('peer says hi: ' + Math.random());
		});
		this.state.peer.on('data', function(data) {
			//data.peerId
			console.log("peer received DATA: " + data);
		});
		this.state.peer.on('stream', stream => {
			component.remoteVideo.srcObject = stream;
		});
		this.state.peer.on('error', function(err){
			console.log(err);
		});
	};

	call = (otherId) => {
		//this.state.peer.connect(otherId);
		//this.state.peer.signal(JSON.parse(otherId));
		console.log("calling other id: ", otherId)
		this.state.peer.signal(otherId);
		

	}
	end = () => {
		console.log("disconnect video called");
		this.setState({ bInitiator: false, bDisconnect: true });
		//simplePeer.destroy();
		this.localVideo.srcObject.getTracks().forEach(track => track.stop());
		if(this.remoteVide != null){
			this.remoteVideo.srcObject.getTracks().forEach(track => track.stop());
		}
		this.state.peer.destroy();
	}
	componentWillUnmount(){
		this.end();
	}
	renderFull = () => {
		if(this.state.full){
			return 'The room is full';
		}
	}
	//manually send a message across
	sayHi(){
		console.log("sending message across");
		this.state.peer.send('hi');
	}
	render(){
		return(
			<div className="video-wrapper .col-2">
				<div className="local-video-wrapper">
					<video autoPlay id="localVideo" className="standardVideo" muted ref={video => (this.localVideo = video)}></video>
				</div>

				<div>
	        		<button id="findNewTable" onClick={this.sayHi}>Say Hi</button>
        		</div>

				{this.renderFull()}
			</div>
		);
	}
}
//<video autoPlay id="remoteVideo" className="standardVideo" ref={video => (this.remoteVideo = video)}></video>
export default Video;
///
/*


			socket.on('full', () => {
			component.setState({ full: true });
		});



		var signalClient = new SimpleSignalClient(socket);
		signalClient.on('ready', function(lastId) {
			if(lastId) {
				signalClient.connect(lastId);
				console.log("client connected to lastId: ", lastId);
			}
		});

		signalClient.on('request', function(request) {
			request.accept()
		});

		signalClient.on('peer', function(peer) {
			component.state.peer = peer;
			//peer //A fully signalled SimplePeer object
			component.state.peer.on('connect', function() {
				console.log('talking');
				component.state.peer.send('clients connected!!')
				component.state.peer.signal('hi there'); 
			});
		});
*/