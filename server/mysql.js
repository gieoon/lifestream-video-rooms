//queries database
const mysql = require('mysql');

//DB location den1.mysql6.gear.host
//DB password   Fi057572-!qe
/*
const connection = mysql.createConnection({
	host: den1.mysql6.gear.host,
	user: 'admin',
	password: 'Fi057572-!qe',
	database: 'mokdb'
});
*/
//-------------------------------------------------------------------
//TEST DB
//localhost
//table = 'sampleTable';
/*
const connection = mysql.createPool({
	connectionLimit: 50,
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'sampleDB'
});
*/
const connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'sampleDB'
});

exports.queryTest = function(){

	connection.query("SELECT * FROM sampleTable", function(error, rows, fields){
		if(!!error){
			console.log("Error in the query");
		}
		else {
			console.log("query successful");
			for(var i = 0; i < rows.length; i++){
				console.log("rows: ", rows[i].Firstname, " ", rows[i].Surname);
			}
			return (rows);
			//console.log("fields: ", fields);
		}
	});
	/*
	connection.getConnection(function(error, tempCount){
		if(!!error){
			//need to release if not using a pool
			connection.release();
			console.log("Error: ", error);
			console.log("Connected: ", connection);
		}
	})
	*/
}



//-----------------------------------------------------------

exports.start = function(){
	//connection.connect()
	//	.catch((err) => console.log("sql connection error caught"));
	console.log("mysql connected");

}

exports.end = function(){
	connection.end();
	console.log("mysql disconnected");
}

exports.queryUserInfo = function(id){
	connection.query('SELECT * FROM users WHERE id LIKE ' + id, 
		function(error, results, fields){
			if(error) throw error;
			//res.send(results);
			return results;
		});
}

exports.queryTableInfo = function(id){
	connection.query('SELECT * FROM tables WHERE id LIKE ' + id),
		function(error, results, fields){
			if(error) throw error;
			return results;
		}
}