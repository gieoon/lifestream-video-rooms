//local modules
var tableInfo = require('./tableInfo');
var roomManager = require('./roomManager');
var connectionBroker = require('./connectionBroker');
var db = require('./mysql');


//local libraries
const express = require('express')();
var fs = require('fs');
var server = require('http').Server(express);  //createServer(express);
//var ExpressPeerServer = require('peer').ExpressPeerServer;
var io = require('socket.io')(server); //initialize with express server
//const app = express();
const port = process.env.PORT || 5000; //PeerServer runs on 9000
//var signalServer = require('simple-signal-server')(io);

//CONSTANTS & GLOBAL VARIABLES
//10
const MAX_ROOM_SIZE = 2; //can also set this to be dynamic and scaling based on users



//------------------------------------------------------------------------------------
//SERVER SETUP
//when suing express, pass the existing express server in.
//either express listens to the port, or socket.io will listen to it.
//var server = app.listen(port, () => console.log('Listening on port  ${ port }'));
//app.listen(port, () => console.log('Listening on port  ${ port }'));
server.listen(port, () => console.log('Listening on port ', { port }));
//OR
//var server = require('http').createServer(app); //for live app i guess?
//var options = { debug: true }
//var peerServer = ExpressPeerServer(server, options);
//set an endpoint
//app.use('/peer', peerServer);




//dictionary for tables, id, table, or just us a plain array?
var tables = {};

var currentConnections = 0;

//------------------------------------------------------------------------------------
var testUser = {
	id: '090129301293',
	username: 'jon snow'
}
var testUser2 = {
	id: '1241351249',
	username: 'Melissa Lee'
}
var testUser3= {
	id: 'GYK241240rEd',
	username: 'Jason Reagan'
}

var testTable = {
	uniqueId: '123456',
	users: [testUser, testUser2, testUser3],
	currentFood: 'kimchi',
	startedAt: Date.now(),
	size: MAX_ROOM_SIZE,
	bInitiator: true
}

tables['123456'] = testTable;

//set to hold unique socketIds
socketIds = {};
currentOffer = {};
//-----------------------------------------------------------------------------
//SIMPLE SIGNAL SERVER
/*
var lastId = null;
signalServer.on('discover', (request) => {
	request.discover(lastId);
	lastId = request.initiator.id;
});
*/

//-----------------------------------------------------------------------------
//SOCKET IO ENDPOINTS
//for realtime communication
//Socket.io enables bidirectional communication channel between client and server
io.on('connection', function(sock) {

	console.log("connection detected: ");
	//add to set if it doesnt exist
	//new Set() = O(n) not scalable...better to use an object lookup.
	// if(!socketIds.hasOwnProperty(sock.id)){
	
	// }

	sock.on('join', function(data){
		console.log("connection joined: ", data);
		
		console.log('Peer available for connection discovered from signalling server, Peer ID: %s', data.roomId);
		sock.join(data.roomId);
		sock.room = data.roomId;
		//get all of the sockets in the room.
		//const sockets = io.of('/').in().adapter.rooms[data.roomId];
		currentConnections = io.engine.clientsCount;
		console.log("sockets.length: ", currentConnections);
		
		if(currentConnections === 1){
			//starting a new room
			console.log("initiating room");
			sock.emit('init', {
				peerId: sock.id,
				initiator: true
			});
		}
		else if(currentConnections < MAX_ROOM_SIZE ) {
			//joining a room
			console.log("sending 'init'");
			io.to(data.roomId).emit('init', {
				peerId: sock.id,
				initiator: false
			});	
		}
		else {
			//start a new room
			sock.emit('newRoom');
		}
	});

	sock.on('signal', (signalData) => {
		console.log("received signal from peer, emitting DESC");
		if(signalData.desc.type == 'offer' || signalData.desc.type == 'answer'){
			currentOffer = signalData;
			console.log('currentOffer: ', currentOffer);
		}
		//io.to(data.peerId).emit('desc', data.desc);
		//THIS IS THE OFFER
		io.emit('desc', signalData.desc);

		
		//var sockets = io.sockets.connected;
		
		//const sockets = io.of('/').in().adapter;
		//console.log("sockets: ", sockets);


		// if(sockets[1]){
		// 	console.log("Proxying signal form peer %s to %s", sockets[0], sockets[1] );

		// 	sockets[1].emit('signal', {
		// 		signal: data.signal,
		// 		peerId: socket.id
		// 	});
		// }


		//send this to other socket now.
	});

	//joined client is requesting an offer to connect to its peer host
	sock.on('requestOffer', () => {
		console.log("REQUEST OFFER RECEIVED");
		//sock.emit('signal', currentOffer);
		sock.emit('offer', currentOffer.desc);
	});

	sock.on('disconnect', () => {
		console.log("disconnect received");
		currentConnections--;
		const roomId = Object.keys(sock.adapter.rooms)[0];
		if(sock.room){
			io.to(sock.room).emit('disconnected');
		}
		sock.disconnect();
	});

});

//-----------------------------------------------------------------------------
//EXPRESS ENDPOINTS
//for request and response communication model
//Express http server gives request response model from client to server.
express.get('/api/getNewTable', (req, res) => {
	//get unique table number
	var newTable = tableInfo.createTable();
	newTable.size = MAX_ROOM_SIZE; //default to this size.
	if(newTable.users.length == 1){
		newTable.bInitiator = true;
	}
	//add to local array of tables, map id to table object
	console.log("server.js received table: ", newTable);
	tables[newTable.uniqueId] = newTable;
	//send this table to client.
	res.send({ table: newTable });

});

express.get('/api/getCurrentTables', (req, res) => {
	//give 27 tables, 9 rows of 3.
	res.send({ tables: tables });
});

express.get('/', function(req, res){
	//tell connection to render this on the page.
	res.send('Hello World!');
	db.start();
	//TODO temporarily disabled
	//db.queryTest();
});


//-----------------------------------------------------------------------------
//PEER SERVER ENDPOINTS
/*
peerServer.on('connection', (id) => {
	console.log("peer connected to server: ", id);
});

peerServer.on('disconnect', (id) => {
	console.log("peer disconnected from server: ", id);
});


/*
	var peersToAdvertise = _.chain(io.sockets.connected) //lodash library
		.values()
		.without(socket)
		.sample(DEFAULT_PEER_COUNT)
		.value();

	console.log('advertising peers', _.map(peersToAdvertise, 'id'));

	peersToAdvertise.forEach(function(socket2){
		console.log('Advertising peer %s to %s', socket.id, socket2.id);
		socket2.emit('peer', {
			peerId: socket.id,
			initiator: true
		});
		socket.emit('peer', {
			peerId: socket2.id,
			initiator: false
		});
	});


var peerServer = expressPeerServer({
	port: 9000,
	path: '/peerjs',
	ssl: {
		key: fs.readFileSync('.././certificates/key.pem', 'utf8'),
		cert: fs.readFileSync('.././certificates/cert.pem', 'utf8')
	}
});

var fs = require('fs');
var PeerServer = require('peer').PeerServer;
 
var server = PeerServer({
  port: 9000,
  ssl: {
    key: fs.readFileSync('/path/to/your/ssl/key/here.key'),
    cert: fs.readFileSync('/path/to/your/ssl/certificate/here.crt')
  }
});
*/